import {CampaignSdkMock} from "./mocks/sdks/campaignSDK";
import {DataBaseMock} from "./mocks/db/config";
import {LogService} from "../app/services/logService";
import {CampaignService} from "../app/services/campaignService";
import * as inversify from "inversify";

const TYPES = {
    CampaignService: "campaignService",
    LogService: "_logService",
    DataBase: "database",
    CampaignSdk: "_campaignSdk"
};

inversify.decorate(inversify.injectable(), CampaignSdkMock);
inversify.decorate(inversify.injectable(), DataBaseMock);
inversify.decorate(inversify.injectable(), CampaignService);
inversify.decorate(inversify.injectable(), LogService);

inversify.decorate(inversify.inject(TYPES.CampaignSdk), LogService, 0);
inversify.decorate(inversify.inject(TYPES.DataBase), LogService, 1);

let container = new inversify.Container();
container.bind(TYPES.CampaignSdk).to(CampaignSdkMock).inSingletonScope();
container.bind(TYPES.DataBase).to(DataBaseMock).inSingletonScope();
container.bind(TYPES.LogService).to(LogService).inSingletonScope();

exports.mockContainer = container;
exports.TYPES = TYPES;