class DataBaseMock {
    constructor() {
        this.models = {
            log: {
                findAll: (filter) => Promise.resolve()
            }
        };
    }
}


exports.DataBaseMock = DataBaseMock;