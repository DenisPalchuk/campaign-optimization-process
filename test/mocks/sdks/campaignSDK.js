class CampaignSdkMock {

    constructor() {
    }

    getAllCampaigns() {
        return Promise.resolve();
    }

    getCampaignStatsByApps(campaignId) {
        return Promise.resolve();
    }

    updateCampaignBID(log) {
        return Promise.resolve();
    }
}

exports.CampaignSdkMock = CampaignSdkMock;