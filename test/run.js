import Jasmine from "jasmine";

let jasmine = new Jasmine();
jasmine.loadConfigFile("test/jasmin.json");
jasmine.execute();