import "reflect-metadata";
import {mockContainer} from "../testContainer";
import {TYPES} from "../testContainer";

describe("Log service test", () => {

    const successArray = [
        {
            campaign_id: "test",
            app_id: "appid1",
            old_bid: 0.5,
            new_bid: 0.5,
            ratio: 0.5,
            created_at: Date.now()
        },
        {
            campaign_id: "test",
            app_id: "appid2",
            old_bid: 0.7,
            new_bid: 0.7,
            ratio: 0.2,
            created_at: Date.now()-2
        },
    ];

    let logService = mockContainer.get(TYPES.LogService);
    let expectedFilter = {where:{campaign_id:"test"}};
    let db = mockContainer.get(TYPES.DataBase);

    describe("get logs by campaign id test", () => {
        it("returns successfully list of logs", (done) => {
            spyOn(db.models.log, "findAll").and.returnValue(Promise.resolve(successArray));
            logService.getLogsByCampaignId("test");
            expect(db.models.log.findAll).toHaveBeenCalledWith(expectedFilter);
            expect(db.models.log.findAll.calls.count()).toBe(1);
            done();
        })
    });

    describe("get all logs", () => {
        it("returns successfully list of logs", (done) => {
            spyOn(db.models.log, "findAll").and.returnValue(Promise.resolve(successArray));
            logService.getLogsByCampaignId("test");
            expect(db.models.log.findAll).toHaveBeenCalled();
            expect(db.models.log.findAll.calls.count()).toBe(1);
            done();
        })
    });
});