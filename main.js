import express from "express";
import Promise from "bluebird";
import {container, TYPES} from "./app/container"
import {campaignControllers} from "./app/controllers/campaignControllers";
import {logControllers} from "./app/controllers/logControllers";
import {logger} from "./app/utils/applicationLogger";

global.Promise = Promise;
let db_settings = {
    dialect: 'mysql',
    define: {
        timestamps: false
    }
};
let db = container.get(TYPES.DataBase);
db.setup(__dirname + "/app/models", "campaign-optimization-process", "root", "root", db_settings);

const port = 3000;
const CAMPAIGNS = "/campaigns";
const LOGS = "/logs";

const app = express();
app.use(CAMPAIGNS, campaignControllers);
app.use(LOGS, logControllers);

app.listen(port, () => {
    logger.info('Example app listening on port 3000!');
});

