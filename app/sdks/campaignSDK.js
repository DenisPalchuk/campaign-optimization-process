import request from "request-promise";

class CampaignSdk {

    constructor() {
        this.BASE_URL = "https://5cd3f999-f49f-4e42-8b8b-173c7185f093.mock.pstmn.io";
    }

    getAllCampaigns() {
        return request({
            uri: this.BASE_URL + "/campaigns",
            json: true
        });
    }

    getCampaignStatsByApps(campaignId) {
        return request({
            uri: this.BASE_URL + `/campaigns/${campaignId}/stats/apps`,
            json: true
        });
    }

    //using it for test, because mock of PUT request are not working on server
    // updateCampaignBID(log) {
    //     return request({
    //         uri: this.BASE_URL + `/campaigns/${log.campaign_id}/stats/apps`,
    //         json: true
    //     });
    // }

    updateCampaignBID(log) {
        return request({
            uri: this.BASE_URL + `/campaigns/${log.campaign_id}/update_bid`,
            method: "PUT",
            body: {
                app_id: log.app_id,
                bid: log.new_bid
            },
            json: true,
        })
            .catch(error => {
                throw new Error(`Can not update data on server: ${error}`);
            });
    }
}

exports.CampaignSdk = CampaignSdk;