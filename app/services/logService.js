import {logger} from "../utils/applicationLogger";

class LogService {
    constructor(campaignSdk,db) {
        this._campaignSdk = campaignSdk;
        this._db = db;
    }

    static isNeedToUpdateBIDforApp(appStat) {
        return appStat.impressions / appStat.opportunities < 0.5;
    }

    static generateLogFromStats(stat,campaignId) {
        /*here I need to add/implement business logic for increasing bid value, but I can not find any info about old
        / value of bid, or place where I can find this info. that's why I decided to hardcode values for test.
        */
        logger.debug(`calculate new bid for appId = ${stat.app_id} and campaignId = ${campaignId}`);
        return {
            campaign_id: campaignId,
            app_id: stat.app_id,
            old_bid: 0.01,
            new_bid: 0.05,
            ratio: stat.impressions / stat.opportunities,
        };
    }

    getAllLogs() {
        logger.debug("get all logs from database");
        return this._db.models.log.findAll()
            .catch((error) => {throw new Error(`Can not get all logs from database: ${error}`)});
    }

    getLogsByCampaignId(campaignId) {
        logger.debug(`get all logs with campaign id = ${campaignId}`);
        let filter = {where:{campaign_id:campaignId}};
        return this._db.models.log.findAll(filter)
            .catch((error) => {
                let error_msg = `Can not get logs by campaignId = ${campaignId}from database: ${error}`;
                throw new Error(error_msg)});
    }

    addNewLogsByStats(stats, campaignId) {
        logger.debug(`add new logs by stats for campaign with id = ${campaignId}`);
        let logs = stats.filter(LogService.isNeedToUpdateBIDforApp)
            .map((stat) => LogService.generateLogFromStats(stat,campaignId));

        return Promise.map(logs, (log) => {
            return this._campaignSdk.updateCampaignBID(log)
                .then(() => { this._db.models.log.create(log); })
                .catch(error => { throw new Error(`Can not insert data to database: ${error}`) });
        },{concurrency:1});
    }
}

exports.LogService = LogService;