import {logger} from "../utils/applicationLogger";

class CampaignService {

    constructor(campaignSdk,logService) {
        this._campaignSdk = campaignSdk;
        this._logService = logService;
        this.DEFAULT_ERROR = "Can not get campaigns from API: ";
        this.STATS_CONCURRENCY = {
            concurrency: 1
        };
    }

    improveCampaignsConfiguration() {
        return this._getAllCampaigns()
            .then(campaigns => {
                this._validateCampaigns(campaigns);
                return this._getLogsToUpdate(campaigns);
            })
    }

    _getAllCampaigns() {
        return this._campaignSdk.getAllCampaigns()
            .then((campaigns) => {
                logger.info(`get ${campaigns.length} campaigns`);
                return campaigns;
            })
            .catch((error) => {
                return Promise.reject(new Error(this.DEFAULT_ERROR + JSON.stringify(error)));
            });
    }

    _getLogsToUpdate(campaigns) {
        return Promise.map(campaigns, (campaign) => this._getLogsForCampaign(campaign), this.STATS_CONCURRENCY)
    }

    _getLogsForCampaign(campaign) {
        return this._getCampaignStatsByApps(campaign.id)
            .then(stats => {
                this._validateStats(stats, campaign.id);

                return this._logService.addNewLogsByStats(stats, campaign.id);
            });
    }

    _getCampaignStatsByApps(campaignId) {
        return this._campaignSdk.getCampaignStatsByApps(campaignId)
            .then(stats => {
                logger.info(`get ${stats.length} for campaign with id = ${campaignId}`);
                return stats;
            })
            .catch((error) => {
                return Promise.reject(new Error(this.DEFAULT_ERROR + JSON.stringify(error)));
            });

    }

    _validateStats(stats) {
        if (!Array.isArray(stats)) {
            logger.info(`stat objects for campaign ${campaign} is not array: ${stats}`);
             throw new Error(this.DEFAULT_ERROR);
        }
    }

    _validateCampaigns(campaigns) {
        if (!Array.isArray(campaigns)) {
            logger.info(`campaign objects is not array: ${campaigns}`);
            throw new Error(this.DEFAULT_ERROR);
        }

        if (campaigns.filter(campaign => !campaign.hasOwnProperty("id")).length > 0) {
            logger.error(`one or more of campaign objects has not id`);
            throw new Error(this.DEFAULT_ERROR);
        }
    }
}

exports.CampaignService = CampaignService;