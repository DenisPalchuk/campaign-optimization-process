import Sequelize from "sequelize";
import filesystem from "fs";


class DataBase {
    constructor() {
        this.modelsPath = "";
        this.models = {};
    }

    setup(modelsPath, dbName, userName, password, settings) {
        this.modelsPath = modelsPath;
        if (arguments.length === 4) {
            this.sequelize = new Sequelize(dbName, userName, password)
        }
        else if (arguments.length === 5) {
            this.sequelize = new Sequelize(dbName,userName, password, settings);
        }

        this.init()
    }

    init() {
        filesystem.readdirSync(this.modelsPath).forEach(name => {
            const model = this.sequelize.import(this.modelsPath+ "/"+name);
            this.models[model.name] = model;
        });
        return this.sequelize.sync();
    }

}

exports.DataBase = DataBase;