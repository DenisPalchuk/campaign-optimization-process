import "reflect-metadata";
import * as inversify from "inversify";
import {LogService} from "./services/logService";
import {CampaignService} from "./services/campaignService";
import {CampaignSdk} from "./sdks/campaignSDK";
import {DataBase} from "./db/config";

const TYPES = {
    CampaignService: "campaignService",
    LogService: "_logService",
    DataBase: "database",
    CampaignSdk: "_campaignSdk"
};

inversify.decorate(inversify.injectable(), CampaignSdk);
inversify.decorate(inversify.injectable(), DataBase);
inversify.decorate(inversify.injectable(), LogService);
inversify.decorate(inversify.injectable(), CampaignService);

inversify.decorate(inversify.inject(TYPES.CampaignSdk), CampaignService,0);
inversify.decorate(inversify.inject(TYPES.LogService), CampaignService,1);
inversify.decorate(inversify.inject(TYPES.CampaignSdk), LogService, 0);
inversify.decorate(inversify.inject(TYPES.DataBase), LogService, 1);

let container = new inversify.Container();
container.bind(TYPES.CampaignSdk).to(CampaignSdk).inSingletonScope();
container.bind(TYPES.DataBase).to(DataBase).inSingletonScope();
container.bind(TYPES.CampaignService).to(CampaignService).inSingletonScope();
container.bind(TYPES.LogService).to(LogService).inSingletonScope();

exports.container = container;
exports.TYPES = TYPES;
