import express from "express";
import {logger} from "../utils/applicationLogger"
import { container, TYPES } from "../container";

let controller = express.Router();
let logService = container.get(TYPES.LogService);

controller.get('/', (req, response) => {
    let campaign_id = req.param('campaign_id');
    if (!campaign_id) {
        logger.info("get all logs");

        logService.getAllLogs()
            .then((logs) => response.type('json').send(logs));
    }
    else {
        logger.info(`get logs by campaign id = ${campaign_id}`);

        logService.getLogsByCampaignId(campaign_id).then((logs) => response.type('json').send(logs));
    }
});
exports.logControllers = controller;