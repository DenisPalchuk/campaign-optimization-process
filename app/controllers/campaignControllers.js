import express from "express";
import {INTERNAL_SERVER_ERROR} from "http-status-codes";
import { container, TYPES } from "../container";

let controller = express.Router();
let campaignService = container.get(TYPES.CampaignService);

controller.put("/configuration", (req, response) => {
  campaignService.improveCampaignsConfiguration()
    .then(() => {
      response.send("OK");
    })
    .catch(error => {
      response.status(INTERNAL_SERVER_ERROR).send(`Error: ${error}`);
    });
});
exports.campaignControllers = controller;