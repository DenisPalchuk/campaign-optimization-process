import winston from "winston2";

function getLogger() {
    return winston;
}

exports.logger = getLogger();