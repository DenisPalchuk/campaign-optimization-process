module.exports =  (sequelize, DataTypes) => {
    return sequelize.define('log', {
        campaign_id: DataTypes.STRING,
        app_id: DataTypes.STRING,
        old_bid: DataTypes.FLOAT,
        new_bid: DataTypes.FLOAT,
        ratio: DataTypes.FLOAT,
        created_at: {
            type:DataTypes.DATE,
            defaultValue: DataTypes.NOW,
        },
    });
};
